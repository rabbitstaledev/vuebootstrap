@extends('layouts.master')

@section('content')
<div class="container">

    <div class="content-wrapper" style="padding: 20px 0;">
        <h1>
            {{ $post->title }}
        </h1>
        
        <div class="content">
            {!! $post->detail !!}
        </div>
    </div>

</div>
@stop

@push('css-stack')
<style>
.content-wrapper img {
    max-width: 100%;
}
</style>
@endpush

@push('js-stack')
@endpush