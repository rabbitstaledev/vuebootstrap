@extends('layouts.master')

@section('content')
<div class="container">

    <div class="recent-items">
        <h1>
            {{ $pageTitle }}
        </h1>
        <?php if (!$posts->isEmpty()) { ?>
            <div class="row">
                <?php foreach ($posts as $post) { ?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <content-post-item
                            title="{{ $post->title }}"
                            excerpt="{{ $post->short_detail }}"
                            thumbnail="{{ $post->rectangleThumbnail }}"
                            permalink="{{ $post->permalink }}"
                        ></content-post-item>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

    {{ $posts->appends(request()->query())->links('vendor.pagination.bootstrap-4') }}

</div>
@stop

@push('css-stack')
@endpush

@push('js-stack')
@endpush