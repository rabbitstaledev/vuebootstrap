<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@section('description')@setting('core::site-description')@show" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@section('title')@setting('core::site-name')@show</title>
    @section('styles')
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('themes/vuebootstrap/css/app.css') }}">
    @show
    @stack('css-stack')
</head>
<body>
    <div id="app">
        @include('partials.header')

        @yield('content')
        
        <div id="footer" style="padding:100px 0; text-align:center; background:#EEE;">
            Footer
        </div>
    </div>
    <?php
        $currentLocale = LaravelLocalization::getCurrentLocale();
    ?>
    @section('scripts')
    <script>
        window.currentLocale   = '<?php echo $currentLocale ?>';
        window.baseApiEndpoint = '<?php echo url("{$currentLocale}/api") ?>/';
    </script>
    <script src="{{ asset('themes/vuebootstrap/js/app.js') }}"></script>
    @show
    @stack('js-stack')
</body>
</html>