const mix = require('laravel-mix');
const WebpackShellPlugin = require('webpack-shell-plugin');
const themeInfo = require('./theme.json');

/**
 * Compile js & scss
 */
mix.js('resources/assets/js/app.js', 'assets/js/app.js')
    .sass('resources/assets/sass/app.scss', 'assets/css/app.css');

mix.copy('assets/js/app.js', '../../public/themes/vuebootstrap/js/app.js');
mix.copy('assets/css/app.css', '../../public/themes/vuebootstrap/css/app.css');

/**
 * Publishing the assets
 */
mix.webpackConfig({
    plugins: [
        new WebpackShellPlugin({ onBuildEnd: [`php ../../artisan stylist:publish ${themeInfo.name}`] }),
    ],
});